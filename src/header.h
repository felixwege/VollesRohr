#ifndef HEADER_H_INCLUDED
#define HEADER_H_INCLUDED

/* cross-platform sleep copied from https://stackoverflow.com/questions/1157209/is-there-an-alternative-sleep-function-in-c-to-milliseconds/28827188#28827188 */
#ifdef WIN32
    #include <windows.h>
#elif _POSIX_C_SOURCE >= 199309L
    #include <time.h>   // for nanosleep
#else
    #include <unistd.h> // for usleep
#endif

void sleep_ms(int milliseconds) // cross-platform sleep function
{
#ifdef WIN32
    Sleep(milliseconds);
#elif _POSIX_C_SOURCE >= 199309L
    struct timespec ts;
    ts.tv_sec = milliseconds / 1000;
    ts.tv_nsec = (milliseconds % 1000) * 1000000;
    nanosleep(&ts, NULL);
#else
    usleep(milliseconds * 1000);
#endif
}
void feld_erstellen(int  **feld, int hoehe, int breite)     /*Funktion, die ein Spielfeld erstellt*/
{
    int h, b, z;
    int rand_max = 10;                                      /*Zeit wird Startwert f�r die Zufallszahl*/
    for(h = 0; h < hoehe; h++)
    {
        for(b = 0; b < breite; b++)
        {
            z = rand() % (rand_max);                            /*z wird zu einer "zuf�lligen" Zahl von 0 bis rand_max*/
            if (z < 4)
                feld[h][b] = -1;                                /*-1 wird mit einer Wahrscheinlichkeit in das Array geschrieben*/
            else
                feld[h][b] = 0;
        }
    }
    feld[0][0] = feld[1][0] = feld[0][1] = feld[0][breite-1] = feld[1][breite-1] = feld[0][breite-2] = 0;   /*Elemente des Arrays beim Start und Ziel werden zu 0 gesetzt*/
    for(h = 0; h < hoehe/3+1; h++)
        feld[h][breite/2] = -1;                                 /*um das Spiel nicht zu einfach zu machen, wird der direkte Weg blockiert*/

    feld_test(feld, hoehe, breite);                             /*die Funktion feld_test wird aufgerufen*/
}

void feld_test(int **feld, int hoehe, int breite)          /*Funktion, die testet, ob eine Rohrverbingung zwischen Start und Ziel m�glich ist*/
{
    int b, h, i;
    feld[0][0] = 9;                                             /*das Feldelement oben links wird zu 9*/
    for(i = 0; i < breite*hoehe; i++)                           /*Vorgang wird so of durchgef�hrt, wie Elemente im Array vorhanden sind*/
    {
        for(h = 0; h < hoehe; h++)
        {
            for(b = 0; b < breite; b++)
            {
                if(feld[h][b] == 9)                             /*wenn ein Element 9 ist, werden umliegende 0en auch zu 9*/
                {
                    if(h-1>=0 && feld[h-1][b] == 0)
                        feld[h-1][b] = 9;

                    if(h+1<hoehe && feld[h+1][b] == 0)
                        feld[h+1][b] = 9;

                    if(b-1>=0 && feld[h][b-1] == 0)
                        feld[h][b-1] = 9;

                    if(b+1<breite && feld[h][b+1] == 0)
                        feld[h][b+1] = 9;
                }
            }
        }
        if(feld[0][breite-1] == 9)                              /*wenn der Zielpunkt 9 ist, werden alle 9en wieder zu 0en*/
        {
            for(h = 0; h < hoehe; h++)
            {
                for(b = 0; b < breite; b++)
                {
                    if(feld[h][b] == 9)
                        feld[h][b] = 0;
                }
            }
            return;                                             /*und die Funktion beendet*/
        }
    }
    feld_erstellen(feld, hoehe, breite);                       /*wenn der Zielpunkt nicht erreicht wurde, wird ein neues Feld erstellt*/
}

void rahmen_malen(int hoehe, int breite, int *screen, int *wasserturm, int *oberflaeche, int *haus, int *hindernis, int *auswahl, int *zeitanzeigehintergrund, SDL_Rect offset)
{
    int h, b;
    SDL_BlitSurface(wasserturm, NULL, screen, &offset);
    for(b = 192; b < breite*64-192; b = b+192)
    {
        offset.x = b;
        SDL_BlitSurface(oberflaeche, NULL, screen, &offset);
    }
    offset.x = (breite*64)-192;
    SDL_BlitSurface(haus, NULL, screen, &offset);

    offset.x = 0;
    offset.y = hoehe*64+192;

    for(b = 0; b < breite; b++)  /* malt in die letzten beiden zeilen Hindernisse */
    {
        SDL_BlitSurface(hindernis, NULL, screen, &offset);
        offset.y += 64;
        SDL_BlitSurface(hindernis, NULL, screen, &offset);
        offset.x += 64;
        offset.y -= 64;
    }
    offset.x = breite*32-192;
    SDL_BlitSurface(zeitanzeigehintergrund, NULL, screen, &offset);
    offset.y += 64;
    SDL_BlitSurface(auswahl, NULL, screen, &offset);
}

void feld_malen(int **feld, int hoehe, int breite, int *screen, int *erde, int *hindernis, int *rohr_1,  int *rohr_2, int *rohr_3, int *rohr_4, int *rohr_5, int *rohr_6, SDL_Rect offset)
{
    int h, b;
    for(h = 0; h < hoehe; h++)
    {
        for(b = 0; b < breite; b++)
        {
            offset.x = 0;
            offset.x = b*64;
            offset.y = h*64+192;
            switch(feld[h][b])
            {
                case -1: SDL_BlitSurface(hindernis, NULL, screen, &offset);
                         break;
                case 0: SDL_BlitSurface(erde, NULL, screen, &offset);
                        break;
                case 1: SDL_BlitSurface(rohr_1, NULL, screen, &offset);
                        break;
                case 2: SDL_BlitSurface(rohr_2, NULL, screen, &offset);
                        break;
                case 3: SDL_BlitSurface(rohr_3, NULL, screen, &offset);
                        break;
                case 4: SDL_BlitSurface(rohr_4, NULL, screen, &offset);
                        break;
                case 5: SDL_BlitSurface(rohr_5, NULL, screen, &offset);
                        break;
                case 6: SDL_BlitSurface(rohr_6, NULL, screen, &offset);
                        break;
            }
        }
    }
    SDL_UpdateRect(screen, 0, 0, 0, 0);
}

void zug_verifizieren(int **feld, int rohr_typ, int y, int x, int hoehe, int breite, int *x_vorh, int *y_vorh)
{
    /*pr�ft, ob Feld Erde ist oder verl�sst die Funktion, wenn man bestimmtes Rohr an den Rand legen will*/
    if(feld[y][x] < 0)
        return;

    int rechts = x+1;
    int links = x-1;
    int oben = y-1;
    int unten = y+1;

    /*es sollen keine Werte au�erhalb des Arrays verglichen werden*/
    if(x == 0)
        links = 0;
    if(x == breite-1)
        rechts = breite-1;
    if(y == 0)
        oben = 0;
    if(y == hoehe-1)
        unten = hoehe-1;

    if(feld[y][x] == 0 || (x == *x_vorh && y == *y_vorh))
    {
        /*rechts und links muss gepr�ft werden*/
        if(rohr_typ == 1)
        {
            if(feld[y][rechts] == 1 || feld[y][rechts] == 3 || feld[y][rechts] == 6
             || feld[y][links] == 1 || feld[y][links] == 4 || feld[y][links] == 5)
            {
                feld[y][x] = 1;
                *x_vorh = x;
                *y_vorh = y;
            }
        }
        /*oben und unten*/
        if(rohr_typ == 2)
        {
            if((y == 0 && x == 0) || feld[oben][x] == 2 || feld[oben][x] == 5 || feld[oben][x] == 6
                                  || feld[unten][x] == 2 || feld[unten][x] == 3 || feld[unten][x] == 4)
            {
                feld[y][x] = 2;
                *x_vorh = x;
                *y_vorh = y;
            }
        }
        /*oben und links*/
        if(rohr_typ == 3)
        {
            if(feld[oben][x] == 2 || feld[oben][x] == 5 || feld[oben][x] == 6
           || feld[y][links] == 1 || feld[y][links] == 4 || feld[y][links] == 5)
            {
                feld[y][x] = 3;
                *x_vorh = x;
                *y_vorh = y;
            }
        }
        /*oben und rechts*/
        if(rohr_typ == 4)
        {
            if((y == 0 && x == 0) || feld[oben][x] == 2 || feld[oben][x] == 5 || feld[oben][x] == 6
                                  || feld[y][rechts] == 1 || feld[y][rechts] == 3 || feld[y][rechts] == 6)
            {
                feld[y][x] = 4;
                *x_vorh = x;
                *y_vorh = y;
            }
        }
        /*unten und rechts*/
        if(rohr_typ == 5)
        {
            if(feld[unten][x] == 2 || feld[unten][x] == 3 || feld[unten][x] == 4
           || feld[y][rechts] == 1 || feld[y][rechts] == 3 || feld[y][rechts] == 6)
            {
                feld[y][x] = 5;
                *x_vorh = x;
                *y_vorh = y;
            }
        }
        /*unten und links*/
        if(rohr_typ == 6)
        {
            if(feld[unten][x] == 2 || feld[unten][x] == 3 || feld[unten][x] == 4
            || feld[y][links] == 1 || feld[y][links] == 4 || feld[y][links] == 5)
            {
                feld[y][x] = 6;
                *x_vorh = x;
                *y_vorh = y;
            }
        }
    }
}
void wasseranimation(int **feld, int hoehe, int breite, int *screen, int *rohr_1w,  int *rohr_2w, int *rohr_3w, int *rohr_4w, int *rohr_5w, int *rohr_6w, int *wasserturm_w, int *haus_w, SDL_Rect offset)
{
    int h = 0;
    int b = 0;
    offset.y = 0;
    offset.x = 0;
    SDL_BlitSurface(wasserturm_w, NULL, screen, &offset);
    SDL_UpdateRect(screen, 0, 0, 0, 0);
    sleep_ms(100);
    do
    {
        offset.y = h*64 + 192;
        offset.x = b*64;
        switch(feld[h][b])
        {
            case 1: SDL_BlitSurface(rohr_1w, NULL, screen, &offset);
                    feld[h][b] = -99;
                    if(b > 0 && feld[h][b-1] > 0)
                        b -= 1;
                    if(b < breite && feld[h][b+1] > 0)
                        b += 1;
                    break;
            case 2: SDL_BlitSurface(rohr_2w, NULL, screen, &offset);
                    feld[h][b] = -99;
                    if(h > 0 && feld[h-1][b] > 0)
                        h -= 1;
                    if(h < hoehe && feld[h+1][b] > 0)
                        h += 1;
                    break;
            case 3: SDL_BlitSurface(rohr_3w, NULL, screen, &offset);
                    feld[h][b] = -99;
                    if(h > 0 && feld[h-1][b] > 0)
                        h -= 1;
                    if(b > 0 && feld[h][b-1] > 0)
                        b -= 1;
                    break;
            case 4: SDL_BlitSurface(rohr_4w, NULL, screen, &offset);
                    feld[h][b] = -99;
                    if(h > 0 && feld[h-1][b] > 0)
                        h -= 1;
                    if(b < breite && feld[h][b+1] > 0)
                        b += 1;
                    break;
            case 5: SDL_BlitSurface(rohr_5w, NULL, screen, &offset);
                    feld[h][b] = -99;
                    if(h < hoehe && feld[h+1][b] > 0)
                        h += 1;
                    if(b< breite && feld[h][b+1] > 0)
                        b += 1;
                    break;
            case 6: SDL_BlitSurface(rohr_6w, NULL, screen, &offset);
                    feld[h][b] = -99;
                    if(h < hoehe && feld[h+1][b] > 0)
                        h += 1;
                    if(b > 0 && feld[h][b-1] > 0)
                        b -= 1;
                    break;
        }
        SDL_UpdateRect(screen, 0, 0, 0, 0);
        sleep_ms(100);
    }while(feld[0][breite-1] >= 0);
    offset.y = 0;
    offset.x = breite*64-192;
    SDL_BlitSurface(haus_w, NULL, screen, &offset);
    SDL_UpdateRect(screen, 0, 0, 0, 0);
    sleep_ms(1000);
}


#endif // HEADER_H_INCLUDED
