#include <stdio.h>
#include <stdlib.h>
#include <SDL/SDL.h>

#include "header.h"

int main(int argc, char *argv[])
{
    int **feld;                                         /*Zeiger auf Zeiger, der sp�ter auf [0][0] des Feldes zeigen soll*/
    int hoehe, breite, h, b;
    int x, y, mouse_x, mouse_y, x_vorh, y_vorh;
    int rohr_typ;
    int zeit_limit;
    int done = 1;
    Uint8 SDL_GetMouseState (int *mouse_x, int *mouse_y);
    Uint32 start, zeit = 0;
    SDL_Surface *screen = NULL, *hindernis = NULL, *erde = NULL, *rohr_1 = NULL, *rohr_2 = NULL, *rohr_3 = NULL, *rohr_4 = NULL, *rohr_5 = NULL, *rohr_6 = NULL,
                                                                 *rohr_1w = NULL, *rohr_2w = NULL, *rohr_3w = NULL, *rohr_4w = NULL, *rohr_5w = NULL, *rohr_6w = NULL,
                                                                 *wasserturm = NULL, *oberflaeche = NULL, *haus = NULL, *auswahl = NULL, *startbildschirm = NULL,
                                                                 *wasserturm_w = NULL, *haus_w = NULL, *zeitanzeigehintergrund = NULL, *ende = NULL;
    SDL_Event event;
    SDL_Rect offset;
    SDL_Rect zeitanzeige;
    SDL_Rect explosion;

    if (SDL_Init(SDL_INIT_VIDEO) == -1)
        exit(1);

    SDL_WM_SetCaption("Volles Rohr", NULL);

    hindernis = SDL_LoadBMP("./Bilder/Hindernis.bmp");
    if (hindernis == NULL)
        exit(1);

    erde = SDL_LoadBMP("./Bilder/Erde.bmp");
    if (hindernis == NULL)
        exit(1);

    rohr_1 = SDL_LoadBMP("./Bilder/Rohr_1.bmp");
    if (rohr_1 == NULL)
        exit(1);

    rohr_2 = SDL_LoadBMP("./Bilder/Rohr_2.bmp");
    if (rohr_2 == NULL)
        exit(1);

    rohr_3 = SDL_LoadBMP("./Bilder/Rohr_3.bmp");
    if (rohr_3 == NULL)
        exit(1);

    rohr_4 = SDL_LoadBMP("./Bilder/Rohr_4.bmp");
    if (rohr_4 == NULL)
        exit(1);

    rohr_5 = SDL_LoadBMP("./Bilder/Rohr_5.bmp");
    if (rohr_5 == NULL)
        exit(1);

    rohr_6 = SDL_LoadBMP("./Bilder/Rohr_6.bmp");
    if (rohr_6 == NULL)
        exit(1);

    rohr_1w = SDL_LoadBMP("./Bilder/Rohr_1w.bmp");
    if (rohr_1w == NULL)
        exit(1);

    rohr_2w = SDL_LoadBMP("./Bilder/Rohr_2w.bmp");
    if (rohr_2w == NULL)
        exit(1);

    rohr_3w = SDL_LoadBMP("./Bilder/Rohr_3w.bmp");
    if (rohr_3w == NULL)
        exit(1);

    rohr_4w = SDL_LoadBMP("./Bilder/Rohr_4w.bmp");
    if (rohr_4w == NULL)
        exit(1);

    rohr_5w = SDL_LoadBMP("./Bilder/Rohr_5w.bmp");
    if (rohr_5w == NULL)
        exit(1);

    rohr_6w = SDL_LoadBMP("./Bilder/Rohr_6w.bmp");
    if (rohr_6w == NULL)
        exit(1);

    wasserturm = SDL_LoadBMP("./Bilder/Wasserturm.bmp");
    if (wasserturm == NULL)
        exit(1);

    oberflaeche = SDL_LoadBMP("./Bilder/Oberflaeche.bmp");
    if (oberflaeche == NULL)
        exit(1);

    haus = SDL_LoadBMP("./Bilder/Haus.bmp");
    if (haus == NULL)
        exit(1);

    auswahl = SDL_LoadBMP("./Bilder/Auswahl.bmp");
    if (auswahl == NULL)
        exit(1);

    startbildschirm = SDL_LoadBMP("./Bilder/Startbildschirm.bmp");
    if (startbildschirm == NULL)
        exit(1);

    wasserturm_w = SDL_LoadBMP("./Bilder/Wasserturm_w.bmp");
    if (wasserturm == NULL)
        exit(1);

    haus_w = SDL_LoadBMP("./Bilder/Haus_w.bmp");
    if (wasserturm == NULL)
        exit(1);

    zeitanzeigehintergrund = SDL_LoadBMP("./Bilder/Zeitanzeigehintergrund.bmp");
    if (zeitanzeigehintergrund == NULL)
        exit(1);

    ende = SDL_LoadBMP("./Bilder/Ende.bmp");
    if (ende == NULL)
        exit(1);

    START:
    offset.w = 64;
    offset.h = 64;
    offset.x = 0;
    offset.y = 0;
    screen = SDL_SetVideoMode(800, 600, 32, SDL_HWSURFACE);
    SDL_BlitSurface(startbildschirm, NULL, screen, &offset);
    SDL_UpdateRect(screen, 0, 0, 0, 0);
    while(done)
    {
        while(SDL_PollEvent(&event))
        {
            if(event.type == SDL_QUIT)
                return 0;

            if(event.type == SDL_MOUSEBUTTONDOWN)
            {
                if(SDL_GetMouseState(NULL, NULL)&SDL_BUTTON_LEFT)
                {
                    SDL_GetMouseState(&mouse_x, &mouse_y);
                    if(mouse_y >= 480 && mouse_y <= 560)
                    {
                        if(mouse_x >= 200 && mouse_x <= 280)
                        {
                            hoehe = 4;
                            breite = 6;
                            zeit_limit = 45000;
                            done = 0;
                        }
                        if(mouse_x >= 320 && mouse_x <= 400)
                        {
                            hoehe = 5;
                            breite = 10;
                            zeit_limit = 40000;
                            done = 0;
                        }
                        if(mouse_x >= 440 && mouse_x <= 520)
                        {
                            hoehe = 6;
                            breite = 14;
                            zeit_limit = 35000;
                            done = 0;
                        }
                        if(mouse_x >= 560 && mouse_x <= 640)
                        {
                            hoehe = 7;
                            breite = 18;
                            zeit_limit = 30000;
                            done = 0;
                        }
                        if(mouse_x >= 680 && mouse_x <= 760)
                        {
                            hoehe = 8;
                            breite = 22;
                            zeit_limit = 25000;
                            done = 0;
                        }
                    }
                }
            }
            if(event.type == SDL_KEYDOWN)
            {
                if(event.key.keysym.sym == SDLK_1)
                {
                    hoehe = 4;
                    breite = 6;
                    zeit_limit = 45000;
                    done = 0;
                }
                if(event.key.keysym.sym == SDLK_2)
                {
                    hoehe = 5;
                    breite = 10;
                    zeit_limit = 40000;
                    done = 0;
                }
                if(event.key.keysym.sym == SDLK_3)
                {
                    hoehe = 6;
                    breite = 14;
                    zeit_limit = 35000;
                    done = 0;
                }
                if(event.key.keysym.sym == SDLK_4)
                {
                    hoehe = 7;
                    breite = 18;
                    zeit_limit = 30000;
                    done = 0;
                }
                if(event.key.keysym.sym == SDLK_5)
                {
                    hoehe = 8;
                    breite = 22;
                    zeit_limit = 25000;
                    done = 0;
                }
                if(event.key.keysym.sym == SDLK_ESCAPE)
                    return 0;
            }
        }
    }
    feld = (int**) malloc((hoehe)*sizeof(int*));        /*Speicher f�r die Spalten reservieren*/
    if (feld == NULL)                                   /*falls kein Speicher vorhanden ist*/
        return;                                         /*wird das Programm beendet*/

    for (h = 0; h < hoehe; h++)
    {
        feld[h] = (int*) malloc((breite)*sizeof(int));  /*f�r jede Spalte Speicher f�r die Zeilen reservieren*/
    }

    srand(time(NULL));
    feld_erstellen(feld, hoehe, breite);

    screen = SDL_SetVideoMode(breite*64, 320+hoehe*64, 32, SDL_HWSURFACE);
    if (screen == NULL)
        exit(1);

    rahmen_malen(hoehe, breite, screen, wasserturm, oberflaeche, haus, hindernis, auswahl, zeitanzeigehintergrund, offset);
    feld_malen(feld, hoehe, breite, screen, erde, hindernis, rohr_1, rohr_2, rohr_3, rohr_4, rohr_5, rohr_6, offset);

    done = 1;
    start = SDL_GetTicks();
    while(done)
    {
        zeit = SDL_GetTicks()-start;
        if(zeit_limit < zeit)
            done = 0;

        zeitanzeige.x = breite*64/2-188;
        zeitanzeige.y = hoehe*64+192+36;
        zeitanzeige.w = zeit*376/zeit_limit;
        zeitanzeige.h = 28;
        SDL_FillRect(screen, &zeitanzeige, SDL_MapRGB(screen->format, 255, 0, 0));
        SDL_UpdateRect(screen, 0, 0, 0, 0);
        while(SDL_PollEvent(&event))
        {
            if(feld[0][breite-1] == 2 || feld[0][breite-1] == 3)
            {
                done = 0;
                wasseranimation(feld, hoehe, breite, screen, rohr_1w,  rohr_2w, rohr_3w, rohr_4w, rohr_5w, rohr_6w, wasserturm_w, haus_w, offset);
            }
            if(event.type == SDL_QUIT)
                return 0;
            if(event.type == SDL_MOUSEBUTTONDOWN)
            {
                if(SDL_GetMouseState(NULL, NULL)&SDL_BUTTON_LEFT)
                {
                    SDL_GetMouseState(&mouse_x, &mouse_y);
                    if(mouse_y-192 >= (hoehe)*64)
                    {
                        if(mouse_x >= (breite*64/2-192) && mouse_x < (breite*64/2-128))
                            rohr_typ = 1;

                        if(mouse_x >= (breite*64/2-128) && mouse_x < (breite*64/2-64))
                            rohr_typ = 2;

                        if(mouse_x >= (breite*64/2-64) && mouse_x < (breite*64/2))
                            rohr_typ = 3;

                        if(mouse_x >= (breite*64/2) && mouse_x < (breite*64/2+64))
                            rohr_typ = 4;

                        if(mouse_x >= (breite*64/2+64) && mouse_x < (breite*64/2+128))
                            rohr_typ = 5;

                        if(mouse_x >= (breite*64/2+128) && mouse_x < (breite*64/2+192))
                            rohr_typ = 6;
                    }
                    if((mouse_y-192) >= 0 && (mouse_y-192) <= (hoehe*64))
                    {
                        x = (mouse_x/64);
                        y = ((mouse_y-192)/64);
                        zug_verifizieren(feld, rohr_typ, y, x, hoehe, breite, &x_vorh, &y_vorh);
                        feld_malen(feld, hoehe, breite, screen, erde, hindernis, rohr_1, rohr_2, rohr_3, rohr_4, rohr_5, rohr_6, offset);
                    }
                }
            }
            if(event.type == SDL_KEYDOWN)
            {
                if(event.key.keysym.sym == SDLK_1)
                    rohr_typ = 1;

                if(event.key.keysym.sym == SDLK_2)
                    rohr_typ = 2;

                if(event.key.keysym.sym == SDLK_3)
                    rohr_typ = 3;

                if(event.key.keysym.sym == SDLK_4)
                    rohr_typ = 4;

                if(event.key.keysym.sym == SDLK_5)
                    rohr_typ = 5;

                if(event.key.keysym.sym == SDLK_6)
                    rohr_typ = 6;

                if(event.key.keysym.sym == SDLK_ESCAPE)
                    return 0;
            }
        }
    }
    if(zeit_limit < zeit)
    {
        explosion.x = breite*64-100;
        explosion.y = 100;
        explosion.w = 20;
        explosion.h = 10;
        int i;
        for(i=0;i<hoehe*32; i++)
        {
            SDL_FillRect(screen, &explosion, SDL_MapRGB(screen->format, 255, 0, 0));
            SDL_UpdateRect(screen, 0, 0, 0, 0);
            explosion.x -= 4;
            explosion.y -= 2;
            explosion.w += 8;
            explosion.h += 4;
            sleep_ms(13);
            SDL_FillRect(screen, &explosion, SDL_MapRGB(screen->format, 255, 150, 0));
            SDL_UpdateRect(screen, 0, 0, 0, 0);
            explosion.x -= 4;
            explosion.y -= 2;
            explosion.w += 8;
            explosion.h += 4;
            sleep_ms(13);
        }
    }
    offset.x = (breite/2-2)*64;
    offset.y = (hoehe+5)*32-64;
    SDL_BlitSurface(ende, NULL, screen, &offset);
    SDL_UpdateRect(screen, 0, 0, 0, 0);
    done = 1;
    while(done)
    {
        while(SDL_PollEvent(&event))
        {
            if(event.type == SDL_QUIT)
                    return 0;
            if(event.type == SDL_KEYDOWN)
            {
                if(event.key.keysym.sym == SDLK_ESCAPE)
                    return 0;
                if(event.key.keysym.sym == SDLK_RETURN)
                    goto START;
            }
        }
    }
    return 0;
}
