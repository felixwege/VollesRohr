# Volles Rohr

Student project during spring 2014.


## Building

- requires SDL-1.2.15
- use ``make`` or compile with ``gcc main.c  `sdl-config --cflags --libs` -o main``
- ignore the warnings ...


## Tutorial

Your task is to supply the nuclear power plant to the right with water from the
water factory to the left. The nuclear power plant needs water to prevent the
ultimate MCA. The nuclear power plant in turn supplies the water factory with
power. The time until the ultimate MCA is limited.

First, select a difficulty level in the opening screen. Then, select a pipe by
pressing 1-6 and click on the cell where you want the pipe to be placed. Repeat
until you have connected the nuclear power plant with the water factory.

<img src="img/level_3.png" width="800"/>
